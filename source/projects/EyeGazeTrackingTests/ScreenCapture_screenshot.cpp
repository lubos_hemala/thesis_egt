#include "pch.h"
#include "TestHelpers.h"

TEST(ScreenCapture, screenshot_default)
{
	const auto services = initialize_services();
	const auto capture = services.screen_capture_manager->capture(services.output0);

	if (capture->moveNext() && capture->hasCurrent())
	{
		const auto current = capture->current();
		std::cout << toString(current->method) << ": " << "out-screenshot-default.png";
		save_to_file(current, services.dxgi_manager, "out-screenshot-default.png");
	}
}

TEST(ScreenCapture, screenshot_gdi)
{
	const auto services = initialize_services<GdiScreenCaptureProviderFactory>();
	const auto capture = services.screen_capture_manager->capture(services.output0);

	if (capture->moveNext() && capture->hasCurrent())
	{
		const auto current = capture->current();
		std::cout << toString(current->method) << ": " << "out-screenshot-gdi.png";
		save_to_file(current, services.dxgi_manager, "out-screenshot-gdi.png");
	}
}

TEST(ScreenCapture, screenshot_dxgi)
{
	const auto services = initialize_services<DxgiScreenCaptureProviderFactory>();
	const auto capture = services.screen_capture_manager->capture(services.output0);

	if (capture->moveNext() && capture->hasCurrent())
	{
		const auto current = capture->current();
		std::cout << toString(current->method) << ": " << "out-screenshot-dxgi.png";
		save_to_file(current, services.dxgi_manager, "out-screenshot-dxgi.png");
	}
}