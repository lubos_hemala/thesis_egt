#include "pch.h"
#include "TestHelpers.h"

TEST(ScreenCapture, ocl_grayscale)
{
	const auto services = initialize_services();
	const auto capture = services.screen_capture_manager->capture(services.output0);

	if (!(capture->moveNext() && capture->hasCurrent()))
		throw std::runtime_error("Unable to capture screen");

	const auto current = capture->current();

	services.ocl_loader->load(std::filesystem::current_path() / ".." / "EyeGazeTracking" / "grayscale.cl");

	const auto device0 = services.ocl_manager->devices().front();
	const auto device0_id = device0->device_id();

	const auto image = services.ocl_memory_dispatcher->dispatch_image(current, device0_id);

	auto kernel = services.ocl_kernel_scheduler->kernel("grayscale_u8", device0_id);
	const auto queue = services.ocl_kernel_scheduler->command_queue(device0_id);
	const auto context = services.ocl_kernel_scheduler->context(device0_id);

	const auto size = current->size();

	auto [result, destination] = TestHelper::buffer<unsigned char>(context, size);

	kernel.setArg(0, *image);
	kernel.setArg(1, destination);

	const auto t1 = std::chrono::high_resolution_clock::now();

	cl::Event kernel_complete;
	queue.enqueueNDRangeKernel(kernel, cl::NullRange, current->range(), cl::NullRange, nullptr, &kernel_complete);

	const auto err_wait = cl::WaitForEvents(std::vector<cl::Event>{kernel_complete});
	if (err_wait != CL_SUCCESS)
		throw std::runtime_error("Waiting for event failed.");

	const auto t2 = std::chrono::high_resolution_clock::now();
	const auto elapsed_mis = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	const auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

	std::cout << "Kernel \"grayscale\" scheduled and executed on device: " << device0->device_name() << " in " << elapsed_mis << "micro seconds (" << elapsed_ms << " ms).";

	const std::string path = "out-grayscale.png";

	TestHelper::map(queue, destination, result.get(), size, [&]()
	{
		save_to_file_grayscale(result.get(), current->width(), current->height(), path);
	});
	
	open_file_when_debugger_present(path);
}