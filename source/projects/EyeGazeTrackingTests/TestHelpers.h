#pragma once
#include <shellapi.h>

struct Services
{
	const std::shared_ptr<IDxgiManager> dxgi_manager;
	const std::shared_ptr<IScreenCaptureProviderFactory> screen_capture_provider_factory;
	const std::shared_ptr<IScreenCaptureManager> screen_capture_manager;
	const std::shared_ptr<DxgiAdapter> adapter0;
	const std::shared_ptr<DxgiOutput> output0;
	const std::shared_ptr<IOclLoader> ocl_loader;
	const std::shared_ptr<IOclManager> ocl_manager;
	const std::shared_ptr<IOclMemoryDispatcher> ocl_memory_dispatcher;
	const std::shared_ptr<IOclKernelDispatcher> ocl_kernel_scheduler;
};

template<typename F>
inline Services initialize_services()
{
	static_assert(std::is_base_of<IScreenCaptureProviderFactory, F>::value, "F must inherit from IScreenCaptureProviderFactory");

	const auto dxgi_manager = std::static_pointer_cast<IDxgiManager>(
		std::make_shared<DxgiManager>());

	const auto adapter0 = dxgi_manager->getAdapters().front();
	const auto output0 = adapter0->getOutputs().front();

	const auto screen_capture_provider_factory = std::static_pointer_cast<IScreenCaptureProviderFactory>(
		std::make_shared<F>(dxgi_manager));

	const auto screen_capture_manager = std::static_pointer_cast<IScreenCaptureManager>(
		std::make_shared<ScreenCaptureManager>(screen_capture_provider_factory));

	const auto ocl_loader = std::static_pointer_cast<IOclLoader>(
		std::make_shared<OclLoader>());

	const auto ocl_manager = std::static_pointer_cast<IOclManager>(
		std::make_shared<OclManager>(dxgi_manager));

	const auto ocl_memory_dispatcher = std::static_pointer_cast<IOclMemoryDispatcher>(
		std::make_shared<OclMemoryDispatcher>(ocl_manager, dxgi_manager));

	const auto ocl_kernel_scheduler = std::static_pointer_cast<IOclKernelDispatcher>(
		std::make_shared<OclKernelDispatcher>(ocl_loader, ocl_manager));

	return Services{
		dxgi_manager,
		screen_capture_provider_factory,
		screen_capture_manager,
		adapter0,
		output0,
		ocl_loader,
		ocl_manager,
		ocl_memory_dispatcher,
		ocl_kernel_scheduler,
	};
}

inline Services initialize_services()
{
	return initialize_services<ScreenCaptureProviderFactory>();
}

inline void log_benchmark(const std::string& prefix, const Benchmark b)
{
	if (b.count == 0)
		std::cout << prefix << b.count << " captures." << std::endl;
	else
		std::cout << prefix << b.count << " captures per second (" << b.ms << "ms on average)." << std::endl;
};

inline void open_file_when_debugger_present(const std::string& path)
{
	const std::wstring p(path.begin(), path.end());
	if (IsDebuggerPresent())
		ShellExecute(nullptr, L"open", p.c_str(), nullptr, nullptr, SW_SHOWDEFAULT);
}

class TestHelper
{
public:
	template<typename T>
	static std::tuple<std::shared_ptr<T[]>, cl::Buffer> buffer(
		const cl::Context& context,
		const size_t size)
	{
		const auto result = std::shared_ptr<T[]>(new T[size]);
		cl_int err_buffer_destination;
		const cl::Buffer destination(
			context,
			CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
			size * sizeof(T),
			result.get(),
			&err_buffer_destination);

		if (err_buffer_destination != CL_SUCCESS)
			throw std::runtime_error("Unable to create destination cl::Buffer.");

		return std::make_tuple(result, destination);
	}

	template<typename T>
	static void map(
		const cl::CommandQueue& queue, 
		const cl::Buffer& memory, 
		T* ptr,
		const size_t size, 
		const std::function<void()>& action)
	{
		cl_int err_map;
		const auto bytes = size * sizeof(T);
		queue.enqueueMapBuffer(memory, CL_TRUE, CL_MAP_READ, 0, bytes, nullptr, nullptr, &err_map);
		if (err_map != CL_SUCCESS)
			throw std::runtime_error("Unable to cl::CommandQueue::enqueueMapBuffer.");

		action();

		const auto err_unmap = queue.enqueueUnmapMemObject(memory, ptr, nullptr, nullptr);
		if (err_unmap != CL_SUCCESS)
			throw std::runtime_error("Unable to cl::CommandQueue::enqueueUnmapMemObject.");
	}
};