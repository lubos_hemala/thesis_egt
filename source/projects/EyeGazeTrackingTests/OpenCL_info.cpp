#include "pch.h"

TEST(OpenCL, Info)
{
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	for (auto &platform : platforms)
	{
		std::cout << "Platform Name: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
		std::cout << "Platform Vendor: " << platform.getInfo<CL_PLATFORM_VENDOR>() << std::endl;
		std::cout << "Platform Extensions: " << platform.getInfo<CL_PLATFORM_EXTENSIONS>() << std::endl;

		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_GPU | CL_DEVICE_TYPE_CPU, &devices);

		for (auto &device : devices) {

			std::cout << "\t\tDevice Name: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
			std::cout << "\t\tDevice Type: " << device.getInfo<CL_DEVICE_TYPE>();
			std::cout << " (GPU: " << CL_DEVICE_TYPE_GPU << ", CPU: " << CL_DEVICE_TYPE_CPU << ")" << std::endl;
			std::cout << "\t\tDevice Vendor: " << device.getInfo<CL_DEVICE_VENDOR>() << std::endl;
			std::cout << "\t\tDevice Vendor Id: " << device.getInfo<CL_DEVICE_VENDOR_ID>() << std::endl;
			std::cout << "\t\tDevice Max Compute Units: " << device.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;
			std::cout << "\t\tDevice Global Memory: " << device.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() << std::endl;
			std::cout << "\t\tDevice Max Clock Frequency: " << device.getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << std::endl;
			std::cout << "\t\tDevice Max Allocatable Memory: " << device.getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>() << std::endl;
			std::cout << "\t\tDevice Local Memory: " << device.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() << std::endl;
			std::cout << "\t\tDevice Max Work-group size: " << device.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
			std::cout << "\t\tDevice Available: " << device.getInfo<CL_DEVICE_AVAILABLE>() << std::endl;
			std::cout << "\t\tDevice Extensions: " << device.getInfo<CL_DEVICE_EXTENSIONS>() << std::endl;
			std::cout << "\t\t===" << std::endl;
		}
		std::cout << std::endl;
	}
}