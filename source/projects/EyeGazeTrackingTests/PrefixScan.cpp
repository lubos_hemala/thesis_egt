#include "pch.h"
#include "TestHelpers.h"
#include <algorithm>

#define NUM_BANKS 16
#define LOG_NUM_BANKS 4

inline int banked(int n)
{
	return (((n) >> NUM_BANKS) + ((n) >> (2 * LOG_NUM_BANKS)));
}

TEST(PrefixScan, prefix_scan)
{
	const auto size = 1024;
	const auto source = std::make_unique<int[]>(size);
	const auto exclusive_cpu = std::make_unique<int[]>(size);

	for (auto i = 0; i < 32; i++)
	{
		

		auto ai = (2 * i + 1) - 1;
		ai += banked(ai);
		auto bi = (2 * i + 2) - 1;
		bi += banked(bi);
		std::cout << ai << "+" << bi << " ";
	}
	std::cout << std::endl;

	srand(size);
	for (auto i = 0; i < size; i++)
	{
		source[i] = rand() % 2;
	}

	auto sum = 0;
	for (auto i = 1; i < size; i++)
	{
		sum += source[i - 1];
		exclusive_cpu[i] = sum;
	}

	std::cout << "cpu: " << exclusive_cpu[size - 1] << std::endl;

	for (auto i = 0; i < size; i++)
	{
		std::cout << source[i] << " ";
	}
	std::cout << std::endl;

	const auto services = initialize_services();
	services.ocl_loader->load(std::filesystem::current_path() / ".." / "EyeGazeTracking" / "prefix_scan.cl");

	for (const auto& device : services.ocl_manager->devices()) {

		const auto device_id = device->device_id();
		
		const auto queue = services.ocl_kernel_scheduler->command_queue(device_id);
		const auto context = services.ocl_kernel_scheduler->context(device_id);

		auto[ptr_out, out] = TestHelper::buffer<int>(context, size);

		cl_int err_buffer;
		auto in = cl::Buffer(context,
			CL_MEM_COPY_HOST_PTR | CL_MEM_READ_ONLY,
			static_cast<size_t>(size) * sizeof(int),
			source.get(),
			&err_buffer);

		auto kernel = services.ocl_kernel_scheduler->kernel("prefix_scan", device_id);

		const auto size_local = size;

		kernel.setArg(0, in);
		kernel.setArg(1, out);
		kernel.setArg(2, sizeof(int) * size, nullptr);
		kernel.setArg(3, size);

		auto wg_multiple = (int)kernel.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(device->device());
		auto lsize = max(32, wg_multiple);
		
		cl::Event evt_kernel;
		const auto err_enq = queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(size/2), cl::NullRange, nullptr, &evt_kernel);
		if (err_enq != CL_SUCCESS)
			throw std::runtime_error("Enqueing kernel failed.");

		const auto err_wait = cl::WaitForEvents(std::vector<cl::Event>{ evt_kernel });
		if (err_wait != CL_SUCCESS)
			throw std::runtime_error("Waiting for kernel failed.");

		TestHelper::map(queue, out, ptr_out.get(), size, [&]()
		{
			std::cout << device->device_name() << ":" << std::endl;
			for (auto i = 0; i < size; i++)
			{
				std::cout << ptr_out[i] << " ";
			}
			std::cout << std::endl;
		});
	}

	
}