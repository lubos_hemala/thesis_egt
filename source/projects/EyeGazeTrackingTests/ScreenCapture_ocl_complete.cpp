#include "pch.h"
#include "TestHelpers.h"

TEST(ScreenCapture, ocl_complete)
{
	const auto services = initialize_services();
	const auto capture = services.screen_capture_manager->capture(services.output0);

	if (!(capture->moveNext() && capture->hasCurrent()))
		throw std::runtime_error("Unable to capture screen");

	const auto current = capture->current();

	services.ocl_loader->load(std::filesystem::current_path() / ".." / "EyeGazeTracking" / "grayscale.cl");
	services.ocl_loader->load(std::filesystem::current_path() / ".." / "EyeGazeTracking" / "sobel.cl");
	services.ocl_loader->load(std::filesystem::current_path() / ".." / "EyeGazeTracking" / "non_maximum_suppression.cl");

	const std::string path_grayscale = "out-grayscale.png";
	const std::string path_sobel_x = "out-sobel_x.png";
	const std::string path_sobel_y = "out-sobel_y.png";
	const std::string path_sobel_gradient = "out-sobel_gradient.png";
	const std::string path_sobel_angle = "out-sobel_angle.png";
	const std::string path_nms = "out-nms.png";

	for (auto j = 0; j < 10; j++) {
		auto i = 0;

		for (const auto& device : services.ocl_manager->devices()) {

			const auto device_id = device->device_id();
			const auto image = services.ocl_memory_dispatcher->dispatch_image(current, device_id);

			const auto queue = services.ocl_kernel_scheduler->command_queue(device_id);
			const auto context = services.ocl_kernel_scheduler->context(device_id);

			const auto size = current->size();

			auto[ptr_grayscale_u8, grayscale_u8] = TestHelper::buffer<unsigned char>(context, size);
			auto[ptr_sobel_x_u8, sobel_x_u8] = TestHelper::buffer<unsigned char>(context, size);
			auto[ptr_sobel_y_u8, sobel_y_u8] = TestHelper::buffer<unsigned char>(context, size);
			auto[ptr_sobel_gradient_u8, sobel_gradient_u8] = TestHelper::buffer<unsigned char>(context, size);
			auto[ptr_sobel_angle_u8, sobel_angle_u8] = TestHelper::buffer<unsigned char>(context, size);
			auto[ptr_nms_u8, nms_u8] = TestHelper::buffer<unsigned char>(context, size);

			auto kernel_grayscale_u8 = services.ocl_kernel_scheduler->kernel("grayscale_u8", device_id);

			const auto t1 = std::chrono::high_resolution_clock::now();

			kernel_grayscale_u8.setArg(0, *image);
			kernel_grayscale_u8.setArg(1, grayscale_u8);

			cl::Event evt_kernel_grayscale_u8;
			queue.enqueueNDRangeKernel(kernel_grayscale_u8, cl::NullRange, current->range(), cl::NullRange, nullptr, &evt_kernel_grayscale_u8);

			auto kernel_sobel_u8 = services.ocl_kernel_scheduler->kernel("sobel_u8", device_id);

			kernel_sobel_u8.setArg(0, grayscale_u8);
			kernel_sobel_u8.setArg(1, sobel_x_u8);
			kernel_sobel_u8.setArg(2, sobel_y_u8);
			kernel_sobel_u8.setArg(3, sobel_gradient_u8);
			kernel_sobel_u8.setArg(4, sobel_angle_u8);

			cl::Event evt_kernel_sobel_u8;
			queue.enqueueNDRangeKernel(kernel_sobel_u8, cl::NullRange, current->range(), cl::NullRange, nullptr, &evt_kernel_sobel_u8);

			auto kernel_nms_u8 = services.ocl_kernel_scheduler->kernel("non_maximum_suppression_u8", device_id);

			kernel_nms_u8.setArg(0, sobel_gradient_u8);
			kernel_nms_u8.setArg(1, sobel_angle_u8);
			kernel_nms_u8.setArg(2, nms_u8);
			kernel_nms_u8.setArg(3, 10);

			cl::Event evt_kernel_nms_u8;
			auto err_enq_nsm = queue.enqueueNDRangeKernel(kernel_nms_u8, cl::NullRange, current->range(), cl::NullRange, nullptr, &evt_kernel_nms_u8);
			if (err_enq_nsm != CL_SUCCESS)
				throw std::runtime_error("Failed to enqueue non_maximum_suppression_u8");

			using namespace std::chrono_literals;

			std::thread t([=]()
			{
				try {
					std::vector<cl_event> e2{evt_kernel_nms_u8()};
					auto err_wait_n = clWaitForEvents(1, &e2[0]);
					if (err_wait_n != CL_SUCCESS)
						throw std::runtime_error("Waiting for event failed.");
				}
				catch (...)
				{
					std::cout << "Error waiting for kernels" << std::endl;
					std::flush(std::cout);
				}
			});

			t.join();

			const auto t2 = std::chrono::high_resolution_clock::now();
			const auto elapsed_mis = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
			const auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

			auto nms_status = evt_kernel_nms_u8.getInfo<CL_EVENT_COMMAND_EXECUTION_STATUS>();
			if (nms_status != CL_COMPLETE)
			{
				throw std::runtime_error("Kernel 'non_maximum_suppression_u8' failed." + std::to_string(nms_status));
			}

			std::cout << device->device_name() << " in " << elapsed_mis << "micro seconds (" << elapsed_ms << " ms)." << std::endl;
			std::flush(std::cout);

			TestHelper::map(queue, grayscale_u8, ptr_grayscale_u8.get(), current->size(), [&]()
			{
				save_to_file_grayscale(ptr_grayscale_u8.get(), current->width(), current->height(), path_grayscale);
			});

			TestHelper::map(queue, sobel_x_u8, ptr_sobel_x_u8.get(), current->size(), [&]()
			{
				save_to_file_grayscale(ptr_sobel_x_u8.get(), current->width(), current->height(), path_sobel_x);
			});

			TestHelper::map(queue, sobel_y_u8, ptr_sobel_y_u8.get(), current->size(), [&]()
			{
				save_to_file_grayscale(ptr_sobel_y_u8.get(), current->width(), current->height(), path_sobel_y);
			});

			TestHelper::map(queue, sobel_gradient_u8, ptr_sobel_gradient_u8.get(), current->size(), [&]()
			{
				save_to_file_grayscale(ptr_sobel_gradient_u8.get(), current->width(), current->height(), path_sobel_gradient);
			});

			TestHelper::map(queue, sobel_angle_u8, ptr_sobel_angle_u8.get(), current->size(), [&]()
			{
				save_to_file_grayscale(ptr_sobel_angle_u8.get(), current->width(), current->height(), path_sobel_angle);
			});

			TestHelper::map(queue, nms_u8, ptr_nms_u8.get(), current->size(), [&]()
			{
				save_to_file_grayscale(ptr_nms_u8.get(), current->width(), current->height(), path_nms);
			});
		}
	}

	open_file_when_debugger_present(path_grayscale);
	open_file_when_debugger_present(path_sobel_x);
	open_file_when_debugger_present(path_sobel_y);
	open_file_when_debugger_present(path_sobel_gradient);
	open_file_when_debugger_present(path_sobel_angle);
	open_file_when_debugger_present(path_nms);
}