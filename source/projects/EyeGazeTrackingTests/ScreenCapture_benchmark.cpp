#include "pch.h"
#include "TestHelpers.h"

TEST(ScreenCapture, benchmark_gdi) 
{
	const auto services = initialize_services<GdiScreenCaptureProviderFactory>();
	const auto gdi = screen_capture_benchmark(services.screen_capture_manager, services.output0);
	log_benchmark("GDI: ", gdi);
}


TEST(ScreenCapture, benchmark_dxgi)
{
	const auto services = initialize_services<DxgiScreenCaptureProviderFactory>();
	const auto dxgi = screen_capture_benchmark(services.screen_capture_manager, services.output0);
	log_benchmark("DXGI: ", dxgi);
}

TEST(ScreenCapture, benchmark_default)
{
	const auto services = initialize_services<ScreenCaptureProviderFactory>();
	const auto factory = screen_capture_benchmark(services.screen_capture_manager, services.output0);
	log_benchmark("", factory);
}

TEST(ScreenCapture, benchmark_both)
{
	const auto servicesGdi = initialize_services<GdiScreenCaptureProviderFactory>();
	const auto gdi = screen_capture_benchmark(servicesGdi.screen_capture_manager, servicesGdi.output0);
	log_benchmark("GDI: ", gdi);

	const auto servicesDxgi = initialize_services<DxgiScreenCaptureProviderFactory>();
	const auto dxgi = screen_capture_benchmark(servicesDxgi.screen_capture_manager, servicesDxgi.output0);
	log_benchmark("DXGI: ", dxgi);
}
