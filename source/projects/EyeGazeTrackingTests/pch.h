//
// pch.h
// Header for standard system include files.
//

#pragma once

#include "../EyeGazeTracking/EyeGazeTracking.h"

#define GTEST_LANG_CXX11 1 // solves gtest error C2039: 'tr1': is not a member of 'std'
#include "gtest/gtest.h"