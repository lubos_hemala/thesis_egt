#pragma once
#include "IDxgiManager.h"
#include "IScreenCaptureProviderFactory.h"
#include "GdiScreenCaptureProvider.h"
#include "DxgiScreenCaptureProvider.h"

class ScreenCaptureProviderFactory : public IScreenCaptureProviderFactory
{
public:
	explicit ScreenCaptureProviderFactory(
		std::shared_ptr<IDxgiManager> dxgiManager
	)
		: _dxgiManager(std::move(dxgiManager)) 
	{

	}

	std::shared_ptr<IScreenCaptureProvider> getProvider(
		const SCREEN_CAPTURE_METHOD method, 
		const std::shared_ptr<DxgiOutput>& output
	) override
	{
		const auto& adapter = _dxgiManager->getAdapter(output->adapter_id);

		switch(method)
		{
			case DXGI:
				return std::static_pointer_cast<IScreenCaptureProvider>(
					std::make_shared<DxgiScreenCaptureProvider>(adapter, output));
			case GDI:
				return std::static_pointer_cast<IScreenCaptureProvider>(
					std::make_shared<GdiScreenCaptureProvider>(adapter, output));
		
		}

		throw std::runtime_error("Can not create provider, method not supported.");
	}

private:
	std::shared_ptr<IDxgiManager> _dxgiManager;
};

