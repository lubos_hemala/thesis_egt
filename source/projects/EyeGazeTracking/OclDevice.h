#pragma once

class OclDevice
{
public:
	OclDevice(
		cl::Platform platform,
		cl::Device device,
		cl::Context context
	)
		: _platform_id(platform()), _device_id(device()), _platform(std::move(platform)), _device(std::move(device)), _context(std::move(context))
	{
		
	}

	cl_platform_id platform_id() const { return _platform_id; }
	cl_device_id device_id() const { return _device_id; }

	cl::Platform platform() const { return _platform; }
	cl::Device device() const { return _device; }
	cl::Context context() const { return _context; }
	cl::CommandQueue command_queue() const { return _command_queue; }

	std::string device_name() const { return _device_name; }

	bool is_cpu() const { return _device_type == CL_DEVICE_TYPE_CPU; }
	bool is_gpu() const { return _device_type == CL_DEVICE_TYPE_GPU; }

	bool is_d3d11_sharing() const { return _is_d3d11_sharing_khr || _is_d3d11_sharing_nv; }

	cl::Kernel kernel(const std::string& name) const
	{
		cl_int err_kernel;
		cl::Kernel k(_program, name.c_str(), &err_kernel);
		if (err_kernel != CL_SUCCESS)
			throw std::runtime_error( std::to_string(err_kernel) + ": Unable to create kernel: " + name + " for the device: " + _device_name + ".");

		return k;
	}

private:
	friend class OclManager;
	friend class OclKernelDispatcher;

	cl_platform_id _platform_id;
	cl_device_id _device_id;

	cl::Platform _platform;
	cl::Device _device;
	cl::Context _context;
	cl::Program _program;
	cl::CommandQueue _command_queue;

	std::string _device_name;
	std::string _device_extensions;

	bool _is_d3d11_sharing_khr = false;
	bool _is_d3d11_sharing_nv = false;

	cl_device_type _device_type = CL_DEVICE_TYPE_DEFAULT;

	bool _is_built = false;

	std::unordered_map<int, std::unordered_set<cl_device_id>> _compatibleAdapters;
	std::unordered_set<int> _incompatibleAdapters;
};

