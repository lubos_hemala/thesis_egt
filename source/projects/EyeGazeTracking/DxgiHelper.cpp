#include "stdafx.h"
#include "DxgiHelper.h"

std::shared_ptr<D3D11_MAPPED_SUBRESOURCE> DxgiHelper::copy_to_main_memory(
	const std::shared_ptr<DxgiScreenCapture>& capture, const std::shared_ptr<IDxgiManager>& dxgi_manager)
{
	auto adapter = dxgi_manager->getAdapter(capture->adapter_id);

	Microsoft::WRL::ComPtr<ID3D11Texture2D> staging;

	const auto& desc = capture->texture_description;
	if (desc.Usage == D3D11_USAGE_STAGING && desc.CPUAccessFlags & D3D11_CPU_ACCESS_READ)
		staging = capture->texture;

	if (!staging)
	{
		D3D11_TEXTURE2D_DESC stagingDesc;
		stagingDesc = desc;
		stagingDesc.BindFlags = 0;
		stagingDesc.Usage = D3D11_USAGE_STAGING;
		stagingDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		stagingDesc.MiscFlags = 0;
		stagingDesc.Height = desc.Height;
		stagingDesc.Width = desc.Width;

		const auto hr_create_texture = adapter->device->CreateTexture2D(&stagingDesc, nullptr, staging.GetAddressOf());

		if (FAILED(hr_create_texture))
			throw std::runtime_error("Unable to ID3D11Device::CreateTexture2D on adapter: " + adapter->toString() + ".");

		adapter->immediateContext->CopyResource(staging.Get(), capture->texture.Get());
	}

	const auto deleter = [adapter, staging](D3D11_MAPPED_SUBRESOURCE* ptr)
	{
		adapter->immediateContext->Unmap(staging.Get(), 0);
		delete ptr;
	};
	auto mapping = std::shared_ptr<D3D11_MAPPED_SUBRESOURCE>(new D3D11_MAPPED_SUBRESOURCE(), deleter);
	const auto hr_map = adapter->immediateContext->Map(staging.Get(), 0, D3D11_MAP_READ, 0, mapping.get());

	if (FAILED(hr_map))
		throw std::runtime_error("Failed to ID3D11DeviceContext::Map on adapter: " + adapter->toString() + ".");

	if (mapping->pData == nullptr)
		throw std::runtime_error("D3D11_MAPPED_SUBRESOURCE::pData is nullptr on adapter: " + adapter->toString() + ".");

	return mapping;
}
