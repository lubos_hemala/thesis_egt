#pragma once
#include "IOclManager.h"
#include "IDxgiManager.h"
#include "OclDevice.h"
#include "OclPlatform.h"

class OclManager : public IOclManager, public std::enable_shared_from_this<IOclManager>
{
public:
	explicit OclManager(
		std::shared_ptr<IDxgiManager> dxgi_manager
	)
		: _dxgi_manager(std::move(dxgi_manager)) 
	{
		
	}

	std::shared_ptr<OclPlatform> platform(cl_platform_id platform_id) override
	{
		initialize();
		
		return _platforms.at(platform_id);
	}

	std::shared_ptr<OclPlatform> platform(cl_device_id device_id) override
	{
		initialize();

		const auto dev = device(device_id);
		return _platforms.at(dev->platform_id());
	}

	std::shared_ptr<OclDevice> device(cl_device_id device_id) override
	{
		initialize();

		return _devices.at(device_id);
	}

	std::vector<std::shared_ptr<OclPlatform>> platforms() override
	{
		initialize();

		return values(_platforms);
	}

	std::vector<std::shared_ptr<OclDevice>> devices() override
	{
		initialize();

		return values(_devices);
	}

protected:
	void initialize();

private:
	bool _is_initialized = false;

	void initialize_platform(const std::shared_ptr<OclPlatform>& ocl_platform);
	void initialize_device(const std::shared_ptr<OclPlatform>& ocl_platform, const std::shared_ptr<OclDevice>& ocl_device);
	void initialize_platform_d3d11_sharing_khr(const std::shared_ptr<OclPlatform>& ocl_platform);
	void initialize_platform_d3d11_sharing_nv(const std::shared_ptr<OclPlatform>& ocl_platform);

	std::map<cl_platform_id, std::shared_ptr<OclPlatform>> _platforms;
	std::map<cl_device_id, std::shared_ptr<OclDevice>> _devices;
	std::shared_ptr<IDxgiManager> _dxgi_manager;
};

