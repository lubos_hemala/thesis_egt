#pragma once
#include "DxgiCommon.h"

enum SCREEN_CAPTURE_METHOD
{
	GDI,
	DXGI,
};

inline std::string toString(SCREEN_CAPTURE_METHOD method)
{
	switch (method)
	{
	case GDI: return "GDI";
	case DXGI: return "DXGI";
	}

	throw std::runtime_error("Not supported SCREEN_CAPTURE_METHOD provided.");
}

class ScreenCapture
{
public:
	dxgi_adapter_id adapter_id;
	dxgi_output_id outputId;
	SCREEN_CAPTURE_METHOD method;

	virtual size_t width() = 0;
	virtual size_t height() = 0;

	size_t size() { return width() * height(); }
	cl::NDRange range() { return cl::NDRange(width(), height()); }

protected:

	ScreenCapture(
		const int adapterId,
		const int outputId,
		const SCREEN_CAPTURE_METHOD method
	)
		: adapter_id(adapterId), outputId(outputId), method(method)
	{
		
	}
};
