#include "stdafx.h"
#include "OclManager.h"

void OclManager::initialize()
{
	if (std::exchange(_is_initialized, true))
		return;

	// Collect available platforms and devices
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	for (const auto& platform : platforms)
	{
		std::vector<cl::Device> devices;
		const auto result = platform.getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &devices);
		if (result != CL_SUCCESS)
			continue;

		// Initialize OclPlatform
		auto platform_id = platform();
		auto ocl_platform = std::make_shared<OclPlatform>(platform);
		
		initialize_platform(ocl_platform);

		_platforms.emplace(platform_id, ocl_platform);

		for (const auto& device : devices)
		{
			// Initialize OclDevice
			auto device_id = device();

			cl_int err_context;
			auto context = cl::Context(device, nullptr, nullptr, nullptr, &err_context);
			if (err_context != CL_SUCCESS)
				continue;

			auto ocl_device = std::make_shared<OclDevice>(platform, device, context);

			initialize_device(ocl_platform, ocl_device);
		
			_devices.emplace(device_id, ocl_device);
		}
	}
}

void OclManager::initialize_platform(const std::shared_ptr<OclPlatform>& ocl_platform)
{
	const auto platform = ocl_platform->platform();
	const auto platform_id = platform();

	cl_int err_platform_name;
	ocl_platform->_platform_name = platform.getInfo<CL_PLATFORM_NAME>(&err_platform_name);
	if (err_platform_name != CL_SUCCESS)
		throw std::runtime_error("Unable to get info CL_PLATFORM_NAME.");

	cl_int err_platform_extensions;
	const auto platform_extensions = platform.getInfo<CL_PLATFORM_EXTENSIONS>(&err_platform_extensions);
	if (err_platform_extensions != CL_SUCCESS)
		throw std::runtime_error("Unable to getInfo CL_platform_EXTENSIONS.");
	ocl_platform->_platform_extensions = platform_extensions;

	// Collect D3D11 sharing capabilities
	ocl_platform->_is_d3d11_sharing_khr = platform_extensions.find("cl_khr_d3d11_sharing") != std::string::npos;
	ocl_platform->_is_d3d11_sharing_nv = platform_extensions.find("cl_nv_d3d11_sharing") != std::string::npos;

	if (ocl_platform->is_d3d11_sharing_khr())
		initialize_platform_d3d11_sharing_khr(ocl_platform);
	if (ocl_platform->is_d3d11_sharing_nv())
		initialize_platform_d3d11_sharing_nv(ocl_platform);
}

void OclManager::initialize_platform_d3d11_sharing_khr(const std::shared_ptr<OclPlatform>& ocl_platform)
{
	const auto platform_id = ocl_platform->platform_id();

	const auto clGetDeviceIDsFromD3D11KHR = (clGetDeviceIDsFromD3D11KHR_fn)
		clGetExtensionFunctionAddressForPlatform(platform_id, "clGetDeviceIDsFromD3D11KHR");
	const auto clCreateFromD3D11Texture2DKHR = (clCreateFromD3D11Texture2DKHR_fn)
		clGetExtensionFunctionAddressForPlatform(platform_id, "clCreateFromD3D11Texture2DKHR");
	const auto clEnqueueAcquireD3D11ObjectsKHR = (clEnqueueAcquireD3D11ObjectsKHR_fn)
		clGetExtensionFunctionAddressForPlatform(platform_id, "clEnqueueAcquireD3D11ObjectsKHR");
	const auto clEnqueueReleaseD3D11ObjectsKHR = (clEnqueueReleaseD3D11ObjectsKHR_fn)
		clGetExtensionFunctionAddressForPlatform(platform_id, "clEnqueueReleaseD3D11ObjectsKHR");

	if (!clGetDeviceIDsFromD3D11KHR || !clCreateFromD3D11Texture2DKHR || !clEnqueueAcquireD3D11ObjectsKHR || !clEnqueueReleaseD3D11ObjectsKHR) 
	{
		ocl_platform->_is_d3d11_sharing_khr = false;
		return;
	}

	ocl_platform->_clGetDeviceIDsFromD3D11KHR = clGetDeviceIDsFromD3D11KHR;
	ocl_platform->_clCreateFromD3D11Texture2DKHR = clCreateFromD3D11Texture2DKHR;
	ocl_platform->_clEnqueueAcquireD3D11ObjectsKHR = clEnqueueAcquireD3D11ObjectsKHR;
	ocl_platform->_clEnqueueReleaseD3D11ObjectsKHR = clEnqueueReleaseD3D11ObjectsKHR;

	for (const auto& adapter : _dxgi_manager->getAdapters()) 
	{
		// Get the number of shared devices
		cl_uint num_devices = 0;
		auto err_shared_d3d11_devices = clGetDeviceIDsFromD3D11KHR(
			platform_id,
			CL_D3D11_DEVICE_KHR,
			adapter->device.Get(),
			CL_ALL_DEVICES_FOR_D3D11_KHR,
			0,
			nullptr,
			&num_devices);

		if (err_shared_d3d11_devices != CL_SUCCESS)
			continue;
			//throw std::runtime_error("Unable to get CL_PREFERRED_DEVICES_FOR_D3D11_KHR for platform: " + ocl_platform->platform_name() + ".");

		if (num_devices == 0)
			continue;

		// Get the actual shared devices
		std::vector<cl_device_id> shared(num_devices);
		err_shared_d3d11_devices = clGetDeviceIDsFromD3D11KHR(
			platform_id,
			CL_D3D11_DEVICE_KHR,
			adapter->device.Get(),
			CL_ALL_DEVICES_FOR_D3D11_KHR,
			num_devices,
			&shared[0],
			&num_devices);

		if (err_shared_d3d11_devices != CL_SUCCESS)
			continue;

		std::unordered_set<cl_device_id> shared_set(shared.begin(), shared.end());
		ocl_platform->_d3d11_sharing_devices_khr.emplace(adapter->adapter_id, shared_set);
	}

	/*cl_context_properties properties[] = {
		CL_CONTEXT_PLATFORM, cl_context_properties(platform_id),
		CL_CONTEXT_D3D11_DEVICE_KHR, cl_context_properties(pD3D11Device),
		CL_CONTEXT_INTEROP_USER_SYNC, CL_FALSE,
		NULL, NULL
	};

	cl_int err_create_context;
	const auto context = clCreateContext(properties, 1, &device, nullptr, nullptr, &err_create_context);
	if (err_create_context != CL_SUCCESS)
		throw std::runtime_error("Unable to clCreateContext for platform: " + ocl_platform->platform_name() + ".");

	ocl_platform->_context = context;*/
}

void OclManager::initialize_platform_d3d11_sharing_nv(const std::shared_ptr<OclPlatform>& ocl_platform)
{

}

void OclManager::initialize_device(const std::shared_ptr<OclPlatform>& ocl_platform, const std::shared_ptr<OclDevice>& ocl_device)
{
	const auto device = ocl_device->device();

	cl_int err_device_name;
	auto device_name = device.getInfo<CL_DEVICE_NAME>(&err_device_name);
	if (err_device_name != CL_SUCCESS)
		throw std::runtime_error("Unable getInfo CL_DEVICE_NAME.");

	ltrim(device_name, [](const int ch) { return !std::isspace(ch); });
	rtrim(device_name, [](const int ch) { return ch == '\0'; });

	ocl_device->_device_name = device_name;

	cl_int err_device_type;
	ocl_device->_device_type = device.getInfo<CL_DEVICE_TYPE>(&err_device_type);
	if (err_device_type != CL_SUCCESS)
		throw std::runtime_error("Unable to getInfo CL_DEVICE_TYPE.");

	cl_int err_device_extensions;
	const auto device_extensions = device.getInfo<CL_DEVICE_EXTENSIONS>(&err_device_extensions);
	if (err_device_extensions != CL_SUCCESS)
		throw std::runtime_error("Unable to getInfo CL_DEVICE_EXTENSIONS.");
	ocl_device->_device_extensions = device_extensions;

	ocl_device->_command_queue = cl::CommandQueue(ocl_device->context(), device);

	// Collect D3D11 sharing capabilities
	ocl_device->_is_d3d11_sharing_khr = device_extensions.find("cl_khr_d3d11_sharing") != std::string::npos;
	ocl_device->_is_d3d11_sharing_nv = device_extensions.find("cl_nv_d3d11_sharing") != std::string::npos;
}