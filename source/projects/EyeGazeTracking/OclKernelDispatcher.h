#pragma once
#include "IOclKernelDispatcher.h"
#include "IOclLoader.h"
#include "IOclManager.h"

class OclKernelDispatcher : public IOclKernelDispatcher, public std::enable_shared_from_this<IOclKernelDispatcher>
{
public:
	explicit OclKernelDispatcher(
		std::shared_ptr<IOclLoader> ocl_loader,
		std::shared_ptr<IOclManager> ocl_manager,
		std::string build_options = ""
	)
		: _ocl_loader(std::move(ocl_loader)), _ocl_manager(std::move(ocl_manager))
	{
		if (!build_options.empty())
			_options = std::move(build_options);
	}

	cl::Kernel kernel(const std::string& kernel_name, cl_device_id target_id) override
	{
		initialize();

		const auto& ocl_device = _ocl_manager->device(target_id);
		return ocl_device->kernel(kernel_name);
	}

	cl::CommandQueue command_queue(cl_device_id target_id) override
	{
		initialize();

		const auto& ocl_device = _ocl_manager->device(target_id);
		return ocl_device->command_queue();
	}

	cl::Context context(cl_device_id target_id) override
	{
		initialize();

		const auto& ocl_device = _ocl_manager->device(target_id);
		return ocl_device->context();
	}

	std::string build_options() override
	{
		return _options;
	}

	void build_options(const std::string options) override
	{
		_options = options;
		initialize();
	}

protected:
	void initialize();

private:
	bool _is_initialized = false;
	
	void initialize_platform(const std::shared_ptr<OclPlatform>& ocl_platform) const;
	void initialize_device(const std::shared_ptr<OclDevice>& ocl_device) const;

	std::shared_ptr<IOclLoader> _ocl_loader;
	std::shared_ptr<IOclManager> _ocl_manager;

	std::string _options = "-cl-std=CL1.2";
};

