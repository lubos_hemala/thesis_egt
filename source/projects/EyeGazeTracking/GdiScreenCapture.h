#pragma once
#include "ScreenCapture.h"

class GdiScreenCapture : public ScreenCapture
{
public:
	GdiScreenCapture(
		const dxgi_adapter_id adapterId,
		const dxgi_output_id outputId,
		const size_t width,
		const size_t height,
		unsigned char* buffer
	)
		: ScreenCapture(adapterId, outputId, GDI), buffer(buffer), _width(width), _height(height)
	{
		
	}

	size_t width() override { return _width; }
	size_t height() override { return _height; }

	unsigned char* buffer;

protected:
	size_t _width;
	size_t _height;
};
