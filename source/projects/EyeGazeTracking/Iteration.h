#pragma once

template<typename T>
class Iteration
{
public:

	void onNext(T item)
	{
		freeze();

		_current = std::make_unique<T>(std::move(item));
		_hasCurrent = true;
	}

	void onCompleted()
	{
		freeze();

		_isCompleted = true;
	}

	void onError(const std::exception& ex)
	{
		freeze();

		_exception = std::make_exception_ptr(ex);
	}

	bool isCompleted() const
	{
		return _isCompleted;
	}

	bool hasCurrent() const
	{
		return _hasCurrent;
	}

	T getCurrent() const
	{
		return *_current;
	}

	std::exception_ptr getException() const
	{
		return _exception;
	}

private:
	void freeze()
	{
		if (_isFrozen)
			throw std::runtime_error("Iteration is already completed.");

		_isFrozen = true;
	}

	bool _isCompleted = false;
	bool _isFrozen = false;
	bool _hasCurrent = false;
	std::exception_ptr _exception = nullptr;
	std::unique_ptr<T> _current = nullptr;
};

