#include "stdafx.h"
#include "OclPlatform.h"

bool OclPlatform::is_d3d11_sharing_khr(dxgi_adapter_id adapter_id, cl_device_id device_id) const
{
	if (!_is_d3d11_sharing_khr)
		return false;

	const auto sharing_it = _d3d11_sharing_devices_khr.find(adapter_id);
	if (sharing_it == _d3d11_sharing_devices_khr.end())
		return false;

	const auto devices = sharing_it->second;
	const auto devices_it = devices.find(device_id);
	return devices_it == devices.end();
}

bool OclPlatform::is_d3d11_sharing_nv(dxgi_adapter_id adapter_id, cl_device_id device_id) const
{
	if (!_is_d3d11_sharing_nv)
		return false;

	const auto sharing_it = _d3d11_sharing_devices_nv.find(adapter_id);
	if (sharing_it == _d3d11_sharing_devices_nv.end())
		return false;

	const auto devices = sharing_it->second;
	const auto devices_it = devices.find(device_id);
	return devices_it == devices.end();
}
