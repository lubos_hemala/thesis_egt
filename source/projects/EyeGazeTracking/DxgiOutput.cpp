#include "stdafx.h"
#include "DxgiOutput.h"

std::atomic<dxgi_output_id> DxgiOutput::_lastId {1};

void DxgiOutput::initialize()
{
	const std::wstring deviceName = desc.DeviceName;
	const std::string n(deviceName.begin(), deviceName.end());
	name.assign(n);

	// Get the number of pixels per logical inch along the screen width
	const auto outputDeviceContext = CreateDCW(desc.DeviceName, nullptr, nullptr, nullptr);
	logicalDpiX = GetDeviceCaps(outputDeviceContext, LOGPIXELSX);
	logicalDpiY = GetDeviceCaps(outputDeviceContext, LOGPIXELSY);
	DeleteDC(outputDeviceContext);

	isRotated = desc.Rotation == DXGI_MODE_ROTATION_ROTATE90 || desc.Rotation == DXGI_MODE_ROTATION_ROTATE270;

	DEVMODEW devMode;
	EnumDisplaySettingsW(desc.DeviceName, ENUM_CURRENT_SETTINGS, &devMode);

	// The width and height of the monitor in pixels
	width = isRotated ? devMode.dmPelsHeight : devMode.dmPelsWidth;
	height = isRotated ? devMode.dmPelsWidth : devMode.dmPelsHeight;
	sizeInBytes = sizeof(unsigned char) * 4 * width * height;
	sizeInBits = sizeInBytes * 8;

	// Relative offset in multiple monitor environment
	offsetX = devMode.dmPosition.x;
	offsetY = devMode.dmPosition.y;
}
