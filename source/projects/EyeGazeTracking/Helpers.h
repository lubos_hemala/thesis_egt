#pragma once

template <typename K, typename V>
inline std::vector<V> values(std::map<K,V> map)
{
	std::vector<V> vector;
	for (const auto& p : map)
		vector.push_back(p.second);

	return vector;
}

inline std::string read(std::ifstream& in) {
	std::stringstream stringStream;
	stringStream << in.rdbuf();
	return stringStream.str();
}

inline std::string read(std::filesystem::path& path)
{
	std::ifstream in(path.string());
	return read(in);
}

inline std::string ocl_error_message(const cl_int error)
{
	switch(error)
	{

	default:
		break;
	}

	throw std::runtime_error(fmt::format("OpenCL status %i not recognized.", error));
}

inline void ocl_verify_status(const cl_int error)
{
	if (error == CL_SUCCESS)
		return;

	const auto message = ocl_error_message(error);
	throw std::runtime_error(message);
}

inline void ltrim(std::string& str, const std::function<bool(int)>& erase_predicate)
{
	str.erase(
		str.begin(),
		std::find_if(
			str.begin(),
			str.end(),
			erase_predicate)
	);
}

inline void rtrim(std::string& str, const std::function<bool(int)>& erase_predicate)
{
	str.erase(
		std::find_if(
			str.begin(),
			str.end(),
			erase_predicate),
		str.end()
	);
}
