#pragma once
#include "OclPlatform.h"
#include "OclDevice.h"

class IOclManager
{
public:
	virtual ~IOclManager() = default;

	virtual std::shared_ptr<OclPlatform> platform(cl_platform_id platform_id) = 0;
	virtual std::shared_ptr<OclPlatform> platform(cl_device_id device_id) = 0;
	virtual std::shared_ptr<OclDevice> device(cl_device_id device_id) = 0;

	virtual std::vector<std::shared_ptr<OclPlatform>> platforms() = 0;
	virtual std::vector<std::shared_ptr<OclDevice>> devices() = 0;
};

