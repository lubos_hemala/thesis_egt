#pragma once
#include "ScreenCapture.h"
#include "Enumerator.h"
#include "ScreenCaptureEnumerator.h"

class IScreenCaptureManager
{
public:
	virtual ~IScreenCaptureManager() = default;

	virtual std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> capture(const std::shared_ptr<DxgiOutput>& output) = 0;
};

class ScreenCaptureManager : public IScreenCaptureManager
{
public:
	explicit ScreenCaptureManager(
		std::shared_ptr<IScreenCaptureProviderFactory> factory
	)
		: _factory(std::move(factory)) 
	{
		
	}

	std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> capture(const std::shared_ptr<DxgiOutput>& output) override
	{
		auto id = output->output_id;
		const auto pair = _enumerators.find(id);
		if (pair != _enumerators.end())
			return pair->second->capture();

		auto enumerator = std::make_shared<ScreenCaptureEnumerator>(output, _factory);
		auto capture = enumerator->capture();

		_enumerators.emplace(id, std::move(enumerator));

		return capture;
	}

private:
	std::shared_ptr<IScreenCaptureProviderFactory> _factory;

	std::mutex _mutex;
	std::map<int, std::shared_ptr<ScreenCaptureEnumerator>> _enumerators;
	
};