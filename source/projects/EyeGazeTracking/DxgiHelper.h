#pragma once
#include "IDxgiManager.h"
#include "DxgiScreenCapture.h"

class DxgiHelper
{
public:
	static std::shared_ptr<D3D11_MAPPED_SUBRESOURCE> copy_to_main_memory(
		const std::shared_ptr<DxgiScreenCapture>& capture,
		const std::shared_ptr<IDxgiManager>& dxgi_manager);
};

