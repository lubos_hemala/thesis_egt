#pragma once

#include "IOclMemoryDispatcher.h"
#include "IOclManager.h"
#include "IDxgiManager.h"
#include "ScreenCapture.h"
#include "DxgiScreenCapture.h"
#include "GdiScreenCapture.h"


class OclMemoryDispatcher : public IOclMemoryDispatcher
{
public:
	OclMemoryDispatcher(
		std::shared_ptr<IOclManager> ocl_manager,
		std::shared_ptr<IDxgiManager> dxgi_manager
	)
		: _ocl_manager(std::move(ocl_manager)), _dxgi_manager(std::move(dxgi_manager))
	{
		
	}

	std::shared_ptr<cl::Image2D> dispatch_image(const std::shared_ptr<ScreenCapture>& screen_capture, cl_device_id target_id) override;

private:
	std::shared_ptr<cl::Image2D> dispatch_dxgi_image(const std::shared_ptr<DxgiScreenCapture>& screen_capture, cl_device_id target_id) const;
	std::shared_ptr<cl::Image2D> dispatch_dxgi_image_using_main_memory(const std::shared_ptr<DxgiScreenCapture>& screen_capture, const cl::Context& context) const;
	std::shared_ptr<cl::Image2D> dispatch_gdi_image(const std::shared_ptr<GdiScreenCapture>& screen_capture, cl_device_id target_id) const;

	std::shared_ptr<IOclManager> _ocl_manager;
	std::shared_ptr<IDxgiManager> _dxgi_manager;
};

