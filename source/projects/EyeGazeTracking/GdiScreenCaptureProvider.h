#pragma once
#include "ScreenCaptureProvider.h"

class GdiScreenCaptureProvider : public ScreenCaptureProvider
{
public:
	GdiScreenCaptureProvider(
		std::shared_ptr<DxgiAdapter> adapter,
		std::shared_ptr<DxgiOutput> output
	)
		: ScreenCaptureProvider(std::move(adapter), std::move(output))
	{

	}

	std::shared_ptr<ScreenCapture> next() override;

	~GdiScreenCaptureProvider() override
	{
		if (_outputDC)
			DeleteDC(_outputDC);
		if (_captureDC)
			DeleteDC(_captureDC);
		if (_bitmap)
			DeleteObject(_bitmap);
	}

protected:
	void initialize() override;

	HDC _outputDC = nullptr;
	HDC _captureDC = nullptr;
	HBITMAP _bitmap = nullptr;
	std::unique_ptr<unsigned char[]> _imageBuffer;
};

