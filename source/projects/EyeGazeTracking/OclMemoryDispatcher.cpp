#include "stdafx.h"
#include "OclMemoryDispatcher.h"
#include "DxgiHelper.h"

std::shared_ptr<cl::Image2D> OclMemoryDispatcher::dispatch_image(
	const std::shared_ptr<ScreenCapture>& screen_capture,
	cl_device_id target_id)
{
	switch (screen_capture->method)
	{
		case DXGI:
		{
			const auto& dxgi_capture = std::static_pointer_cast<DxgiScreenCapture>(screen_capture);
			return dispatch_dxgi_image(dxgi_capture, target_id);
		}
		case GDI:
		{
			const auto gdi_capture = std::static_pointer_cast<GdiScreenCapture>(screen_capture);
			return dispatch_gdi_image(gdi_capture, target_id);
		}
	}

	throw std::runtime_error("Capture method not supported.");
}

std::shared_ptr<cl::Image2D> OclMemoryDispatcher::dispatch_dxgi_image(
	const std::shared_ptr<DxgiScreenCapture>& screen_capture,
	cl_device_id target_id) const
{
	const auto ocl_platform = _ocl_manager->platform(target_id);
	const auto ocl_device = _ocl_manager->device(target_id);
	const auto context = ocl_device->context();
	const auto queue = ocl_device->command_queue();

	if (ocl_platform->is_d3d11_sharing_khr(screen_capture->adapter_id, target_id))
	{
		cl_int err_texture_image;
		const auto texture_image = ocl_platform->_clCreateFromD3D11Texture2DKHR(
			context(), CL_MEM_READ_ONLY, screen_capture->texture.Get(), 0, &err_texture_image);

		if (err_texture_image != CL_SUCCESS)
			return dispatch_dxgi_image_using_main_memory(screen_capture, context);

		const auto err_enqueue_acquire = ocl_platform->_clEnqueueAcquireD3D11ObjectsKHR(
			queue(), 1, &texture_image, 0, nullptr, nullptr);

		if (err_enqueue_acquire != CL_SUCCESS)
			return dispatch_dxgi_image_using_main_memory(screen_capture, context);

		const auto deleter = [queue, texture_image, ocl_platform](cl::Image2D* ptr)
		{
			const auto err_enqueue_release = ocl_platform->_clEnqueueReleaseD3D11ObjectsKHR(
				queue(), 1, &texture_image, 0, nullptr, nullptr);

			delete ptr;

			if (err_enqueue_release != CL_SUCCESS)
				throw std::runtime_error("Unable to call clEnqueueReleaseD3D11ObjectsKHR_fn.");
			
		};
		return std::shared_ptr<cl::Image2D>(new cl::Image2D(texture_image), deleter);
	}

	if (ocl_platform->is_d3d11_sharing_nv(screen_capture->adapter_id, target_id))
	{

	}

	return dispatch_dxgi_image_using_main_memory(screen_capture, context);
}

std::shared_ptr<cl::Image2D> OclMemoryDispatcher::dispatch_dxgi_image_using_main_memory(
	const std::shared_ptr<DxgiScreenCapture>& screen_capture,
	const cl::Context& context) const
{
	const auto mapped = DxgiHelper::copy_to_main_memory(screen_capture, _dxgi_manager);

	const auto width = screen_capture->texture_description.Width;
	const auto height = screen_capture->texture_description.Height;

	cl_int err_image;
	const cl::ImageFormat format(CL_BGRA, CL_UNORM_INT8);
	const auto image = std::make_shared<cl::Image2D>(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		format,
		width,
		height,
		0,
		mapped->pData,
		&err_image
		);

	if (err_image != CL_SUCCESS)
		throw std::runtime_error("Unable to create cl::Image2D.");

	return image;
}

std::shared_ptr<cl::Image2D> OclMemoryDispatcher::dispatch_gdi_image(
	const std::shared_ptr<GdiScreenCapture>& screen_capture,
	cl_device_id target_id) const
{
	const auto ocl_platform = _ocl_manager->platform(target_id);
	const auto ocl_device = _ocl_manager->device(target_id);
	const auto context = ocl_device->context();

	cl_int err_image;
	const cl::ImageFormat format(CL_BGRA, CL_UNORM_INT8);
	const auto image = std::make_shared<cl::Image2D>(
		context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		format,
		screen_capture->width(),
		screen_capture->height(),
		0,
		screen_capture->buffer,
		&err_image
	);

	if (err_image != CL_SUCCESS)
		throw std::runtime_error("Unable to create cl::Image2D.");

	return image;
}
