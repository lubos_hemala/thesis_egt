#pragma once
#include "DxgiAdapter.h"

class IDxgiManager
{
public:
	virtual ~IDxgiManager() = default;

	virtual std::vector<std::shared_ptr<DxgiAdapter>> getAdapters() = 0;
	virtual const std::shared_ptr<DxgiAdapter>& getAdapter(int adapterId) const = 0;
};

