#pragma once
#include "stdafx.h"
#include "ScreenCaptureManager.h"
#include "DxgiHelper.h"

#include <FreeImage.h>

struct Benchmark final
{
	double count;
	double ms;
};

inline Benchmark screen_capture_benchmark(
	const std::shared_ptr<IScreenCaptureManager>& scm, 
	const std::shared_ptr<DxgiOutput>& output)
{
	auto capture = scm->capture(output);

	auto count = 0;
	const auto t1 = std::chrono::high_resolution_clock::now();
	while (capture->moveNext())
	{
		count++;
		if (std::chrono::steady_clock::now() - t1 > std::chrono::seconds(20))
			break;
	}

	if (count == 0)
		return Benchmark{ 0, 0 };

	const auto t2 = std::chrono::steady_clock::now();
	const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();

	const auto ms = duration / static_cast<double>(count);
	const auto c = 1000 / ms;

	return Benchmark { c, ms };
}

inline void save_to_file_rgb(
	unsigned char* buffer,
	const size_t width,
	const size_t height,
	const std::string& path,
	const bool top_down = true
)
{
	const auto dib = FreeImage_ConvertFromRawBits(reinterpret_cast<BYTE*>(
		buffer),
		width,
		height,
		static_cast<int>(width) * 4,
		32,
		FI_RGBA_RED_MASK,
		FI_RGBA_GREEN_MASK,
		FI_RGBA_BLUE_MASK,
		top_down ? TRUE : FALSE);

	FreeImage_Save(FIF_PNG, dib, path.c_str());
	FreeImage_Unload(dib);
}

inline void save_to_file_grayscale(
	unsigned char* buffer,
	const size_t width,
	const size_t height,
	const std::string& path,
	const bool top_down = true
)
{
	const auto dib = FreeImage_ConvertFromRawBits(reinterpret_cast<BYTE*>(
		buffer),
		width, 
		height,
		width, 
		8, 
		FI_RGBA_RED_MASK, 
		FI_RGBA_GREEN_MASK, 
		FI_RGBA_BLUE_MASK, 
		top_down ? TRUE : FALSE);

	FreeImage_Save(FIF_PNG, dib, path.c_str());
	FreeImage_Unload(dib);
}

inline void save_to_file(
	const std::shared_ptr<ScreenCapture>& capture, 
	const std::shared_ptr<IDxgiManager>& dxgi_manager, 
	const std::string& path)
{
	switch (capture->method)
	{
		case DXGI:
		{
			const auto& dxgiCapture = std::static_pointer_cast<DxgiScreenCapture>(capture);
			const auto& desc = dxgiCapture->texture_description;

			const auto& mapping = DxgiHelper::copy_to_main_memory(dxgiCapture, dxgi_manager);
			const auto buffer = reinterpret_cast<unsigned char *>(mapping->pData);

			save_to_file_rgb(buffer, desc.Width, desc.Height, path);
			return;
		}
		case GDI:
		{
			const auto gdi_capture = std::static_pointer_cast<GdiScreenCapture>(capture);
			save_to_file_rgb(gdi_capture->buffer, gdi_capture->width(), gdi_capture->height(), path);

			return;
		}
	}

	throw std::runtime_error("Capture method not supported.");
}