#include "stdafx.h"
#include "DxgiManager.h"
#include "DxgiAdapter.h"

using Microsoft::WRL::ComPtr;

void DxgiManager::initialize()
{
	auto hr = CreateDXGIFactory1(IID_PPV_ARGS(&_factory1));
	if (FAILED(hr))
		throw std::runtime_error("Unable to create IDXGIFactory1");

	// Enumerate available video adapters
	ComPtr<IDXGIAdapter1> adapter1;
	for (UINT i = 0; _factory1->EnumAdapters1(i, &adapter1) != DXGI_ERROR_NOT_FOUND; ++i)
	{
		ComPtr<IDXGIAdapter> adapter;
		hr = adapter1.As(&adapter);
		if (FAILED(hr))
		{
			std::cerr << "Unable to obtain IDXGIAdapter from IDXGIAdapter1 " << i << "." << std::endl;
			continue;
		}

		DXGI_ADAPTER_DESC1 adapter_desc1;
		hr = adapter1->GetDesc1(&adapter_desc1);
		if (FAILED(hr))
		{
			std::cerr << "Unable to obtain description of IDXGIOutput1 " << i << ".";
			continue;
		}

		try 
		{
			auto a = DxgiAdapter(adapter, adapter1, adapter_desc1);
		
			// Enumerate outputs of each video adapter
			ComPtr<IDXGIOutput> output;
			ComPtr<IDXGIOutput1> output1;
			for (UINT j = 0; adapter1->EnumOutputs(j, &output) != DXGI_ERROR_NOT_FOUND; ++j)
			{
				hr = output.As(&output1);
				if (FAILED(hr)) {
					std::cerr << "Unable to obtain IDXGIOutput1 from IDXGIOutput " << i << "-" << j << std::endl;
					continue;
				}

				DXGI_OUTPUT_DESC output_desc;
				hr = output1->GetDesc(&output_desc);
				if (FAILED(hr))
				{
					std::cerr << "Unable to obtain description of IDXGIOutput1 " << i << ".";
					continue;
				}

				auto o = DxgiOutput(a.adapter_id, output, output1, output_desc);
				a.addOutput(std::move(o));
			}

			addAdapter(std::move(a));

		}
		catch (const std::runtime_error& ex)
		{
			
		}
	}
}
