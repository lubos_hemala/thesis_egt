#include "stdafx.h"
#include "GdiScreenCaptureProvider.h"
#include "GdiScreenCapture.h"

void GdiScreenCaptureProvider::initialize()
{
	// Create a device context for the name
	_outputDC = CreateDCA(output->name.c_str(), nullptr, nullptr, nullptr);
	if (!_outputDC)
		throw std::runtime_error("Unable to CreateDCA for output: " + output->toString() + ".");

	// Create a compatible memory device context
	_captureDC = CreateCompatibleDC(_outputDC);
	if (!_outputDC)
		throw std::runtime_error("Unable to CreateCompatibleDC for output: " + output->toString() + ".");

	_bitmap = CreateCompatibleBitmap(_outputDC, output->width, output->height);
	if (!_outputDC)
		throw std::runtime_error("Unable to CreateCompatibleBitmap for output: " + output->toString() + ".");

	_imageBuffer = std::make_unique<unsigned char[]>(output->sizeInBytes);
}

std::shared_ptr<ScreenCapture> GdiScreenCaptureProvider::next()
{
	initializeNext();

	// Selecting an object into the specified DC
	const auto originalBmp = SelectObject(_captureDC, _bitmap);

	if (BitBlt(_captureDC, 0, 0, output->width, output->height, _outputDC, 0, 0, SRCCOPY | CAPTUREBLT) == FALSE)
	{
		// The screen can not be captured, restore
		SelectObject(_captureDC, originalBmp);
		return nullptr;
	}

	BITMAPINFOHEADER header;
	memset(&header, 0, sizeof(header));

	header.biSize = sizeof(BITMAPINFOHEADER);
	header.biWidth = output->width;
	header.biHeight = -output->height;
	header.biPlanes = 1;
	header.biBitCount = output->pixelSizeInBits;
	header.biCompression = BI_RGB;
	header.biSizeImage = output->pixelSizeInBits;

	GetDIBits(_outputDC, _bitmap, 0, output->height, _imageBuffer.get(), reinterpret_cast<BITMAPINFO *>(&header), DIB_RGB_COLORS);

	SelectObject(_captureDC, originalBmp);

	const auto sc = std::static_pointer_cast<ScreenCapture>(
		std::make_shared<GdiScreenCapture>(
			output->adapter_id, 
			output->output_id, 
			static_cast<size_t>(output->width), 
			static_cast<size_t>(output->height), 
			_imageBuffer.get()));

	return sc;
}
