#include "stdafx.h"
#include "DxgiScreenCaptureProvider.h"
#include "DxgiScreenCapture.h"

using Microsoft::WRL::ComPtr;

void DxgiScreenCaptureProvider::initialize()
{
	const auto hr_duplicate_output = output->output1->DuplicateOutput(adapter->device.Get(), &_outputDuplication);

	if (FAILED(hr_duplicate_output))
	{
		if (hr_duplicate_output == DXGI_ERROR_UNSUPPORTED)
			throw std::runtime_error("The output " + output->toString() + " does not support desktop duplication (DXGI_ERROR_UNSUPPORTED).");

		throw std::runtime_error("Unable to duplicate desktop for output " + output->toString()  + ".");
	}
}

std::shared_ptr<ScreenCapture> DxgiScreenCaptureProvider::next()
{
	initializeNext();

	DXGI_OUTDUPL_FRAME_INFO frameInfo;
	ComPtr<IDXGIResource> resource;

	while (true) {

		if (_isAcquired) 
		{
			const auto hr_release_frame = _outputDuplication->ReleaseFrame();
			if (FAILED(hr_release_frame))
				throw std::runtime_error("Output " + output->toString() + ": IDXGIOutputDuplication::ReleaseFrame failed.");

			_isAcquired = false;
		}

		const auto hr_acquire_frame = _outputDuplication->AcquireNextFrame(100, &frameInfo, resource.GetAddressOf());
		if (FAILED(hr_acquire_frame))
			throw std::runtime_error("Output " + output->toString() + ": IDXGIOutputDuplication::AcquireNextFrame failed.");

		_isAcquired = true;

		if (frameInfo.AccumulatedFrames == 0)	
			continue;
			
		break;
	}

	ComPtr<ID3D11Texture2D> texture;
	const auto hr_qi_texture = resource.Get()->QueryInterface(__uuidof(ID3D11Texture2D), reinterpret_cast<void **>(texture.GetAddressOf()));
	if (FAILED(hr_qi_texture))
		throw std::runtime_error("Output " + output->toString() + ": IDXGIResource::QueryInterface for ID3D11Texture2D failed.");

	D3D11_TEXTURE2D_DESC textureDesc;
	texture->GetDesc(&textureDesc);

	const auto sc = std::static_pointer_cast<ScreenCapture>(
		std::make_shared<DxgiScreenCapture>(output->adapter_id, output->output_id, texture, textureDesc));

	return sc;
}
