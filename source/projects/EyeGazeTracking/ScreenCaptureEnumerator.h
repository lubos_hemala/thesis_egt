#pragma once
#include "Enumerator.h"
#include "IScreenCaptureProviderFactory.h"

class ScreenCaptureEnumerator
{
public:
	virtual ~ScreenCaptureEnumerator() = default;

	ScreenCaptureEnumerator(
		std::shared_ptr<DxgiOutput> output,
		std::shared_ptr<IScreenCaptureProviderFactory> factory
	)
		: _output(std::move(output)), _factory(std::move(factory))
	{
		initialize();
	}

	std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> capture();

private:
	void initialize();

	static void next(std::shared_ptr<IScreenCaptureProvider> provider, Iteration<std::shared_ptr<ScreenCapture>>& it)
	{
		try
		{
			const auto next = provider->next();
			it.onNext(next);
		}
		catch (const std::exception& ex)
		{
			it.onError(ex);
		}
	};

	std::shared_ptr<DxgiOutput> _output;
	std::shared_ptr<IScreenCaptureProviderFactory> _factory;

	std::chrono::steady_clock::time_point _prev = std::chrono::steady_clock::now();

	std::shared_ptr<IScreenCaptureProvider> _dxgi;
	std::shared_ptr<IScreenCaptureProvider> _gdi;

	std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> _dxgiEnumerator;
	std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> _gdiEnumerator;

	std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> _enumerator;

	const int _reinitializeAfter = 10;
};

