#pragma once
#include "DxgiAdapter.h"
#include "IScreenCaptureProvider.h"

class ScreenCaptureProvider : public IScreenCaptureProvider
{
public:
	ScreenCaptureProvider(
		std::shared_ptr<DxgiAdapter> adapter, 
		std::shared_ptr<DxgiOutput> output
	)
		:  adapter(std::move(adapter)), output(std::move(output))
	{

	}

protected:
	virtual void initialize() = 0;

	void initializeNext()
	{
		if (_isInitialized)
			return;

		initialize();
		_isInitialized = true;
	}

	std::shared_ptr<DxgiAdapter> adapter;
	std::shared_ptr<DxgiOutput> output;

private:
	bool _isInitialized = false;
};

