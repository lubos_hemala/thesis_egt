#pragma once
#include "DxgiCommon.h"

class OclPlatform
{
public:
	explicit OclPlatform(
		cl::Platform platform
	)
		: _platform_id(platform()), _platform(std::move(platform))
	{
		
	}

	cl_platform_id platform_id() const { return _platform_id; }

	cl::Platform platform() const { return _platform; }

	std::string platform_name() const { return  _platform_name; }

	bool is_d3d11_sharing_khr() const { return _is_d3d11_sharing_khr; }
	bool is_d3d11_sharing_nv() const { return _is_d3d11_sharing_nv; }
	bool is_d3d11_sharing() const { return _is_d3d11_sharing_khr || _is_d3d11_sharing_nv; }

	bool is_d3d11_sharing_khr(dxgi_adapter_id adapter_id, cl_device_id device_id) const;
	bool is_d3d11_sharing_nv(dxgi_adapter_id adapter_id, cl_device_id device_id) const;

protected:
	friend class OclManager;
	friend class OclKernelDispatcher;
	friend class OclMemoryDispatcher;

	cl_platform_id _platform_id;

	cl::Platform _platform;

	std::string _platform_name;
	std::string _platform_extensions;

	bool _is_d3d11_sharing_khr = false;
	bool _is_d3d11_sharing_nv = false;

	clGetDeviceIDsFromD3D11KHR_fn _clGetDeviceIDsFromD3D11KHR = nullptr;
	clCreateFromD3D11Texture2DKHR_fn _clCreateFromD3D11Texture2DKHR = nullptr;
	clEnqueueAcquireD3D11ObjectsKHR_fn _clEnqueueAcquireD3D11ObjectsKHR = nullptr;
	clEnqueueReleaseD3D11ObjectsKHR_fn _clEnqueueReleaseD3D11ObjectsKHR = nullptr;

	std::unordered_map<dxgi_adapter_id, std::unordered_set<cl_device_id>> _d3d11_sharing_devices_khr;
	std::unordered_map<dxgi_adapter_id, std::unordered_set<cl_device_id>> _d3d11_sharing_devices_nv;
};

