#pragma once
#include "DxgiCommon.h"

class DxgiOutput
{
public:
	DxgiOutput(
		const int adapterId,
		Microsoft::WRL::ComPtr<IDXGIOutput> output,
		Microsoft::WRL::ComPtr<IDXGIOutput1> output1,
		const DXGI_OUTPUT_DESC desc
	) : 
		adapter_id(adapterId), 
		output_id(_lastId++), 
		output(std::move(output)), 
		output1(std::move(output1)), 
		desc(desc)
	{
		initialize();
	}

	dxgi_adapter_id adapter_id;
	dxgi_output_id output_id;
	Microsoft::WRL::ComPtr<IDXGIOutput> output;
	Microsoft::WRL::ComPtr<IDXGIOutput1> output1;
	DXGI_OUTPUT_DESC desc;

	std::string name;
	bool isRotated = false;
	int width = 0;
	int height = 0;
	int sizeInBytes = 0;
	int sizeInBits = 0;
	int pixelSizeInBits = sizeof(unsigned char) * 32;
	int logicalDpiX = 96;
	int logicalDpiY = 96;
	long offsetX = 0;
	long offsetY = 0;

	std::string toString() const { return fmt::format("%s (%i-%i)", name, adapter_id, output_id); }

private:
	void initialize();

	static std::atomic<dxgi_output_id> _lastId;
};
