#pragma once
#include "ScreenCapture.h"

class IOclMemoryDispatcher
{
public:
	virtual ~IOclMemoryDispatcher() = default;

	virtual std::shared_ptr<cl::Image2D> dispatch_image(
		const std::shared_ptr<ScreenCapture>& screen_capture, 
		cl_device_id target_id) = 0;
};
