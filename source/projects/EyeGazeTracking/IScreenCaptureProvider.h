#pragma once
#include "ScreenCapture.h"

class IScreenCaptureProvider
{
public:
	virtual ~IScreenCaptureProvider() = default;

	virtual std::shared_ptr<ScreenCapture> next() = 0;
};
