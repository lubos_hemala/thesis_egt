#include "stdafx.h"
#include "DxgiAdapter.h"

std::atomic<dxgi_adapter_id> DxgiAdapter::_lastId {1};

void DxgiAdapter::initialize()
{
	const auto featureLevels = &_featureLevels[0];
	const auto hr_create_device = D3D11CreateDevice(
		adapter.Get(),
		D3D_DRIVER_TYPE_UNKNOWN,
		nullptr,
		0,
		featureLevels,
		_featureLevels.size(),
		D3D11_SDK_VERSION,
		&device,
		&featureLevel,
		&immediateContext);

	if (FAILED(hr_create_device))
		throw std::runtime_error("Unable to D3D11CreateDevice for adapter " + toString() + ".");
}
