#pragma once
#include "IDxgiManager.h"
#include "DxgiAdapter.h"

class DxgiManager : public IDxgiManager
{
public:
	DxgiManager()
	{
		initialize();
	}

	std::vector<std::shared_ptr<DxgiAdapter>> getAdapters() override
	{
		return values(_adapters);
	}

	const std::shared_ptr<DxgiAdapter>& getAdapter(const int adapterId) const override
	{
		const auto it = _adapters.find(adapterId);
		if (it != _adapters.end())
			return it->second;

		throw std::runtime_error(fmt::format("Adapter %i not found.", adapterId));
	}

private:
	void initialize();

	void addAdapter(DxgiAdapter adapter)
	{
		auto id = adapter.adapter_id;
		_adapters.emplace(id, std::make_shared<DxgiAdapter>(std::move(adapter)));
	}

	Microsoft::WRL::ComPtr<IDXGIFactory1> _factory1;

	std::map<int, std::shared_ptr<DxgiAdapter>> _adapters;
};
