#pragma once
#include "ScreenCapture.h"

class DxgiScreenCapture : public ScreenCapture
{
public:
	DxgiScreenCapture(
		const dxgi_adapter_id adapterId,
		const dxgi_output_id outputId,
		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture,
		const D3D11_TEXTURE2D_DESC textureDesc
	)
		: ScreenCapture(adapterId, outputId, DXGI), texture(std::move(texture)), texture_description(std::move(textureDesc))
	{

	}

	size_t width() override { return texture_description.Width; }
	size_t height() override { return texture_description.Height; }

	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
	D3D11_TEXTURE2D_DESC texture_description;
};

