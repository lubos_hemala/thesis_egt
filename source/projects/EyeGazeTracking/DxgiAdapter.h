#pragma once
#include "DxgiCommon.h"
#include "DxgiOutput.h"

class DxgiAdapter
{
public:
	DxgiAdapter(
		Microsoft::WRL::ComPtr<IDXGIAdapter> adapter,
		Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter1,
		const DXGI_ADAPTER_DESC1 desc
	) : 
		adapter_id(_lastId++), 
		adapter(std::move(adapter)), 
		adapter1(std::move(adapter1)), 
		desc(desc)
	{
		initialize();
	}

	dxgi_adapter_id adapter_id;
	Microsoft::WRL::ComPtr<IDXGIAdapter> adapter;
	Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter1;
	DXGI_ADAPTER_DESC1 desc;

	Microsoft::WRL::ComPtr<ID3D11Device> device;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext> immediateContext;
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_9_1;

	std::vector<std::shared_ptr<DxgiOutput>> getOutputs() const
	{
		return values(_outputs);
	}

	void addOutput(DxgiOutput output)
	{
		auto id = output.output_id;
		_outputs.emplace(id, std::make_shared<DxgiOutput>(std::move(output)));
	}

	std::string toString() const
	{
		const std::wstring description = desc.Description;
		const std::string str(description.begin(), description.end());
		return fmt::format("%s (%i)", str, adapter_id);
	}

private:
	void initialize();

	std::map<int, std::shared_ptr<DxgiOutput>> _outputs;

	static std::atomic<dxgi_adapter_id> _lastId;

	std::vector<D3D_FEATURE_LEVEL> _featureLevels
	{
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1,
	};
};
