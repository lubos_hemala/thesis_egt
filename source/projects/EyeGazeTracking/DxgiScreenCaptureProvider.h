#pragma once
#include "ScreenCaptureProvider.h"

class DxgiScreenCaptureProvider : public ScreenCaptureProvider
{
public:
	DxgiScreenCaptureProvider(
		std::shared_ptr<DxgiAdapter> adapter, 
		std::shared_ptr<DxgiOutput> output
	)
		: ScreenCaptureProvider(std::move(adapter), std::move(output))
	{

	}

	std::shared_ptr<ScreenCapture> next() override;

protected:
	void initialize() override;

private:

	bool _isAcquired = false;

	Microsoft::WRL::ComPtr<IDXGIOutputDuplication> _outputDuplication;
};

