#pragma once
#include "Iteration.h"

template<typename T>
class IEnumerator
{
public:
	virtual ~IEnumerator() = default;

	virtual bool isInitialized() = 0;
	virtual bool isCompleted() = 0;

	virtual bool moveNext() = 0;
	virtual bool hasCurrent() = 0;
	virtual T current() = 0;
};

template<typename T>
class Enumerator : public IEnumerator<T>
{
public:
	explicit Enumerator(
		std::function<void(Iteration<T>&)> next
	)
		: _next(std::move(next))
	{
		
	}

	bool isInitialized() override
	{
		return _isInitialized;
	}

	bool isCompleted() override
	{
		return _isCompleted;
	}

	bool hasCurrent() override
	{
		return _hasCurrent;
	}

	T current() override
	{
		if (!_isInitialized)
			throw std::runtime_error("Enumerator has not yet been moved.");

		if (hasCurrent())
			return _iteration.getCurrent();

		auto exception = _iteration.getException();
		if (exception)
			std::rethrow_exception(exception);

		throw std::runtime_error("Enumerator is already exhausted.");
	}

	bool moveNext() override
	{
		if (_isCompleted)
			return false;

		_iteration = Iteration<T>();
		_next(_iteration);

		_hasCurrent = _iteration.hasCurrent();
		_isCompleted = _iteration.isCompleted() || _iteration.getException();

		if (!_isInitialized)
			_isInitialized = true;

		return _hasCurrent;
	}

private:
	bool _isInitialized = false;
	bool _isCompleted = false;
	bool _hasCurrent = false;

	std::function<void(Iteration<T>& iteration)> _next;
	Iteration<T> _iteration;
};


