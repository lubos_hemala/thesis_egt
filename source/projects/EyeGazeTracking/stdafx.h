// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

// reference additional headers your program requires here

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.hpp>
#include <CL/cl_d3d11.h>

#include <dxgi.h>
#include <dxgi1_2.h>
#include <d3d11.h>
#include <d3dcommon.h>

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

#include <wrl.h>

#include <fmt/format.h>

#include <future>
#include <filesystem>
#include <functional>
#include <utility>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <unordered_set>
#include <cctype>

#include "Helpers.h"
//#include "../../../external/RxCpp/Rx/v2/src/rxcpp/rx.hpp"
