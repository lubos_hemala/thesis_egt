#include "stdafx.h"
#include "ScreenCaptureEnumerator.h"

void ScreenCaptureEnumerator::initialize()
{
	_dxgi = _factory->getProvider(DXGI, _output);
	_gdi = _factory->getProvider(GDI, _output);

	auto dxgiCapture = [=](Iteration<std::shared_ptr<ScreenCapture>>& it) {
		next(_dxgi, it);
	};

	_dxgiEnumerator = std::make_shared<Enumerator<std::shared_ptr<ScreenCapture>>>(dxgiCapture);

	auto gdiCapture = [=](Iteration<std::shared_ptr<ScreenCapture>>& it) {
		next(_gdi, it);
	};

	_gdiEnumerator = std::make_shared<Enumerator<std::shared_ptr<ScreenCapture>>>(gdiCapture);

	_prev = std::chrono::steady_clock::now();
}

std::shared_ptr<Enumerator<std::shared_ptr<ScreenCapture>>> ScreenCaptureEnumerator::capture()
{
	if (_enumerator)
		return _enumerator;

	const auto cap = [=](Iteration<std::shared_ptr<ScreenCapture>>& it)
	{
		if (std::chrono::steady_clock::now() > _prev + std::chrono::seconds(_reinitializeAfter)) 
		{
			// Reinitialize in case a mode has changed
			initialize();
		}

		if (!_dxgiEnumerator->isCompleted() && _dxgiEnumerator->moveNext())
		{
			it.onNext(_dxgiEnumerator->current());
			return;
		}

		if (!_gdiEnumerator->isCompleted() && _gdiEnumerator->moveNext())
		{
			it.onNext(_gdiEnumerator->current());
			return;
		}

		it.onCompleted();
	};

	_enumerator = std::make_shared<Enumerator<std::shared_ptr<ScreenCapture>>>(cap);

	return _enumerator;
}
