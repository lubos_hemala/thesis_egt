#pragma once
#include "IDxgiManager.h"
#include "IScreenCaptureProviderFactory.h"
#include "GdiScreenCaptureProvider.h"

class GdiScreenCaptureProviderFactory : public IScreenCaptureProviderFactory
{
public:
	explicit GdiScreenCaptureProviderFactory(
		std::shared_ptr<IDxgiManager> dxgiManager
	)
		: _dxgiManager(std::move(dxgiManager))
	{

	}

	std::shared_ptr<IScreenCaptureProvider> getProvider(
		SCREEN_CAPTURE_METHOD method,
		const std::shared_ptr<DxgiOutput>& output
	) override
	{
		auto adapter = _dxgiManager->getAdapter(output->adapter_id);

		return std::static_pointer_cast<IScreenCaptureProvider>(
			std::make_shared<GdiScreenCaptureProvider>(adapter, output));
	}

private:
	std::shared_ptr<IDxgiManager> _dxgiManager;
};
