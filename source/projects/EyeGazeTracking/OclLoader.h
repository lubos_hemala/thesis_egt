#pragma once
#include "IOclLoader.h"

class OclLoader : public IOclLoader
{
public:
	void load_cwd() override;
	void load(const std::filesystem::path& source) override;

	cl::Program::Sources sources() override;

private:
	void load_file(const std::filesystem::path& source);

	std::vector<std::filesystem::path> _source_files;
	std::string _sources;
};

