#pragma once
#include "IScreenCaptureProvider.h"
#include "DxgiOutput.h"
#include "ScreenCapture.h"

class IScreenCaptureProviderFactory
{
public:
	virtual ~IScreenCaptureProviderFactory() = default;

	virtual std::shared_ptr<IScreenCaptureProvider> getProvider(
		SCREEN_CAPTURE_METHOD method,
		const std::shared_ptr<DxgiOutput>& output) = 0;
};
