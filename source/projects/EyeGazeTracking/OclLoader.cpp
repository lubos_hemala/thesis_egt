#include "stdafx.h"
#include "OclLoader.h"

using namespace std::filesystem;

void OclLoader::load_cwd()
{
	load(current_path());
}

void OclLoader::load(const path& source)
{
	if (!exists(source))
		throw std::runtime_error("Path does not exists: " + source.string());

	if (is_directory(source)) 
	{
		for (const auto& entry : directory_iterator(source))
		{
			const auto& path = entry.path();
			if (is_directory(path)) {
				load(path);
				continue;
			}

			if (path.extension() == ".cl")
				load_file(path);
		}
	}
	else 
	{
		load_file(source);
	}
}

void OclLoader::load_file(const path& source)
{
	if (!is_regular_file(source) || source.extension() != ".cl")
		throw std::runtime_error("Path is not an OpenCL source file: " + source.string());

	const auto file = canonical(source);

	_source_files.push_back(file);

	std::ifstream ifs(file.string());
	if (!ifs)
		throw std::runtime_error("Unable to open OpenCL file: " + file.string());

	const std::string content((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

	_sources += content + "\n";
}

cl::Program::Sources OclLoader::sources()
{
	if (_source_files.empty())
		throw std::runtime_error("OclLoader has no sources loaded.");

	const auto sources = cl::Program::Sources(1, std::make_pair(_sources.c_str(), _sources.size() + 1));
	return sources;
}
