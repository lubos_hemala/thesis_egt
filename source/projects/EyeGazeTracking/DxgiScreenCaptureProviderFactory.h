#pragma once
#include "ScreenCaptureProviderFactory.h"

class DxgiScreenCaptureProviderFactory : public IScreenCaptureProviderFactory
{
public:
	explicit DxgiScreenCaptureProviderFactory(
		std::shared_ptr<IDxgiManager> dxgiManager
	)
		: _dxgiManager(std::move(dxgiManager))
	{

	}

	std::shared_ptr<IScreenCaptureProvider> getProvider(
		SCREEN_CAPTURE_METHOD method,
		const std::shared_ptr<DxgiOutput>& output
	) override
	{
		auto adapter = _dxgiManager->getAdapter(output->adapter_id);

		return std::static_pointer_cast<IScreenCaptureProvider>(
			std::make_shared<DxgiScreenCaptureProvider>(adapter, output));
	}

private:
	std::shared_ptr<IDxgiManager> _dxgiManager;
};

