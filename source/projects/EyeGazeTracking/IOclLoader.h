#pragma once
class IOclLoader
{
public:
	virtual ~IOclLoader() = default;

	virtual void load_cwd() = 0;
	virtual void load(const std::filesystem::path& source) = 0;

	virtual cl::Program::Sources sources() = 0;
};