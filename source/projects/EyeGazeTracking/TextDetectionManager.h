#pragma once
#include "IDxgiManager.h"
#include "IOclKernelDispatcher.h"
#include "IOclMemoryDispatcher.h"

class TextDetectionManager
{
public:
	TextDetectionManager(
		std::shared_ptr<IDxgiManager> dxgiManager,
		std::shared_ptr<IOclKernelDispatcher> oclManager,
		std::shared_ptr<IOclMemoryDispatcher> oclMemoryDispatcher
	) : 
		_dxgiManager(std::move(dxgiManager)), 
		_oclManager(std::move(oclManager)), 
		_oclMemoryDispatcher(std::move(oclMemoryDispatcher))
	{

	}

private:
	std::shared_ptr<IDxgiManager> _dxgiManager;
	std::shared_ptr<IOclKernelDispatcher> _oclManager;
	std::shared_ptr<IOclMemoryDispatcher> _oclMemoryDispatcher;
};

