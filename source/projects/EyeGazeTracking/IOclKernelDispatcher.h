#pragma once
class IOclKernelDispatcher
{
public:
	virtual ~IOclKernelDispatcher() = default;

	virtual std::string build_options() = 0;
	virtual void build_options(std::string options) = 0;

	virtual cl::Kernel kernel(const std::string& kernel_name, cl_device_id target_id) = 0;
	virtual cl::CommandQueue command_queue(cl_device_id target_id) = 0;
	virtual cl::Context context(cl_device_id target_id) = 0;
};