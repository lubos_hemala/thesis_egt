#include "stdafx.h"
#include "OclKernelDispatcher.h"

void OclKernelDispatcher::initialize()
{
	if (std::exchange(_is_initialized, true))
		return;

	// Compile program for each device
	for (const auto& ocl_platform : _ocl_manager->platforms())
		initialize_platform(ocl_platform);

	for (const auto& ocl_device : _ocl_manager->devices())
		initialize_device(ocl_device);
}

void OclKernelDispatcher::initialize_platform(const std::shared_ptr<OclPlatform>& ocl_platform) const
{
}

void OclKernelDispatcher::initialize_device(const std::shared_ptr<OclDevice>& ocl_device) const
{
	const auto device = ocl_device->device();
	const auto ocl_platform = _ocl_manager->platform(ocl_device->platform_id());

	const auto sources = _ocl_loader->sources();
	const auto context = ocl_device->context();

	cl_int err_program;
	cl::Program program(context, sources, &err_program);
	if (err_program != CL_SUCCESS)
		throw std::runtime_error("Unable to create cl::Program for device " + ocl_device->device_name() + ".");

	const std::vector<cl::Device> devices{ device };
	const auto err_build = program.build(devices, _options.c_str());
	if (err_build != CL_SUCCESS)
	{
		cl_int err_build_log;
		const auto build_log = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device, &err_build_log);
		if (err_build_log != CL_SUCCESS)
			throw std::runtime_error("Program build error on device: " + ocl_device->device_name() + ". Unable to getBuildInfo CL_PROGRAM_BUILD_LOG.");

		throw std::runtime_error("Program build error on device: " + ocl_device->device_name() + ".\n" + build_log);
	}

	while (true)
	{
		const auto status = program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device);
		switch (status)
		{
		case CL_BUILD_IN_PROGRESS:
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			std::cout << "waiting for build.";
			continue;
		case CL_BUILD_NONE:
			throw std::runtime_error("The OpenCL program has not been built.");
		case CL_BUILD_ERROR:
			throw std::runtime_error("The OpenCL program build had previously failed.");
		}

		break;
	}

	ocl_device->_program = std::move(program);
}
