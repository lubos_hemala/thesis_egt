# README #

# Install Vcpkg
# https://github.com/Microsoft/vcpkg

vcpkg install opencl opencv freeimage fmt boost gtest --triplet x64-windows
vcpkg integrate install

# Install Visual Studio Build Tools 2019 (C++ Build Tools)
https://visualstudio.microsoft.com/cs/downloads/#build-tools-for-visual-studio-2019

# Use Visual Studio 2019 for running tests in EyeGazeTrackingTests

# git

git submodule init
git submodule update

# OpenCL

1) Wrappers for OpenCL C objects are reference-counted, therefore there is no need to use std::shared_ptr.
2) To obtain cl_platform_id or cl_device_id from cl::Platform or cl::Device the ()-operator is used.